# AIOT服务快速部署

本项目可以帮助开发人员快速部署一个物联网平台。

使用者基础技能要求：
1. docker、 docker-compose常见命令
2. 物联网入门知识
3. 数据库入门知识

使用者进阶技能要求：
1. 数据分析技术
2. 物理仿真技术
3. 流式数据湖仓技术

## 基础集成包

AIOT基础集成包有三部份：物联网平台、数据挖掘工具、物理仿真工具。

物联网平台选用Thingsboard。

数据挖掘工具选用Knime。

物联网仿真工具选用Octave。

